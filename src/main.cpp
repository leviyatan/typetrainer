#include <iostream>
#include <string>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "typetable.h"

int main(){
	std::cout << "Typetrainer" << std::endl;

	std::string types[]{
		"Bug", "Dark", "Dragon", "Electric", "Fairy", 
		"Fighting", "Fire", "Flying", "Ghost", "Grass",
		"Ground", "Ice", "Normal", "Poison", "Psychic",
		"Rock", "Steel", "Water"
	};

	srand(time(NULL));
	bool running = true;

	while (running){
		int attack = rand()%18;
		int defend1 = rand()%18;
		int defend2 = rand()%18;
		while (defend1 == defend2){
			defend2 = rand()%18;
		}

		std::cout << "Attacker: " << types[attack] << std::endl;
		std::cout << "Defender: " << types[defend1] << "/" << types[defend2] << std::endl;
		std::cout << "Is it:\n- Immune (1)\n- Barely effective (2)\n- Not very effective (3)\n- Effective (4)\n- Super effective (5)\n- Extremely effective (6)\n" << std::endl;

		float effectiveness = typetable[attack][defend1] * typetable[attack][defend2];

		int guess;
		float check;
		std::cin >> guess;
		switch (guess){
			case 1	: check = 0; break;
			case 2	: check = 0.25; break;
			case 3	: check = 0.5; break;
			case 4	: check = 1; break;
			case 5	: check = 2; break;
			case 6	: check = 4; break;
		}

		if (check == effectiveness){
			std::cout << "Correct! It's ";
		} else {
			std::cout << "Wrong! It's ";
		}

		if (effectiveness == 0) std::cout << "Immune" << std::endl;
		if (effectiveness == 0.25) std::cout << "Barely effective" << std::endl;
		if (effectiveness == 0.5) std::cout << "Not very effective" << std::endl;
		if (effectiveness == 1) std::cout << "Effective" << std::endl;
		if (effectiveness == 2) std::cout << "Super effective" << std::endl;
		if (effectiveness == 4) std::cout << "Extremely effective" << std::endl;

		std::cout << "Continue? Yes(1), No(2)" << std::endl;
		int again;
		std::cin >> again;
		if (again == 2) running = false;
	}
	return 0;
}