CC = g++
OBJS = ./src/*.cpp
LINK_INCLUDES = -I./include/
OBJ_NAME = typetrainer
all : $(OBJS)
	$(CC) $(OBJS) $(LINK_INCLUDES) -o $(OBJ_NAME)
clean:
	$(RM) count *.o *~ typetrainer
